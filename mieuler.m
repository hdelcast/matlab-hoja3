function [T,Y] = mieuler(f, intv, y0, N) 

a = intv(1);
b = intv(2);
h = (b - a)/N;

t = a;
y = y0;

T = t;
Y = y;

for k=1:N
    l = f(t,y);
    
    t = t + h;
    y = y + h * l;

    T = [T,t];
    Y = [Y,y];
end
end
