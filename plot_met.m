function plot_met(t,y,f,nom_met,intv,y0,N)

figure;
set(gca,'FontSize',16);
hold on 
plot(y(1,:),y(2,:),'r-+')
hold off
grid on
s=sprintf('Diagrama de fase (Corazón), \n met=%s, intv=[%g %g], \n y0=[%g %g], N=%g',nom_met,intv,y0,N);
title(s)

end
