y0 = [0; 2]
intv = [0, 2*pi]
N = [100, 400, 800]
f = @(t, y)[y(2); -16*y(1) + 4*sin(2*t)]


[t, y] = mieuler(f, intv, y0, N(2));
plot_met(t,y,f,'mieuler',intv,y0,N(2));

disp(y(end));

y1_euler = [-0.0049; 2.6449];

new_y = [y, y1];

hold on;
plot(y1(1,:),y1(2,:),'b-*')

[t, y] = mieuler(f, intv, y0, N(2));
