function[T,Y] = mieulermej(f, intv, y0, N)
 
a = min(intv);
b = max(intv);
h = (b - a)/N;

t = a;
y = y0;

T = t;
Y = y;

for k=1:N

    l1 = f(t,y);
    l2 = f(t + h, y + h * l1);

    t = t + h;
    y = y + (h/2) * (l1 + l2);

    T = [T,t];
    Y = [Y,y];
end

end
